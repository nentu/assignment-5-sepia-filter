#include "image_manager.h"
#include "bmp_file_stream_io.h"
#include "file_io.h"
#include "io_result_enums.h"
#include "transform_image.h"
#include "utils.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

char *read_errors[] = {
        "OK",
        "Invalid path",
        "Invalid signature",
        "Invalid bits",
        "Invalid header"
};

char *write_errors[] = {
        "OK",
        "Invalid path",
        "Error during writing the file"
};

int rotate_file(char *source_name, char *res_name, char *angle_arg) {
    uint16_t angle = atoi(angle_arg);


    struct image image;

    enum read_status read_status = read_image(&image, source_name);
    if (read_status != READ_OK) {
        printf("Read error: %s\n", read_errors[read_status]);
        image_free(&image);
        return 2;
    }
    // image_print("read image", image);



    struct image new_image = rotate(image, angle);


    enum write_status write_status = write_image(&new_image, res_name);
    if (write_status != WRITE_OK) {
        printf("Write error: %s\n", write_errors[write_status]);
        image_free(&image);
        image_free(&new_image);
        return 3;
    }
    // image_print("write image", new_image);

    image_free(&image);
    image_free(&new_image);

}

int filter_file(char *source_name, char *res_name, char *filter_type) {
    uint16_t type = atoi(filter_type);


    struct image image;

    enum read_status read_status = read_image(&image, source_name);
    if (read_status != READ_OK) {
        printf("Read error: %s\n", read_errors[read_status]);
        image_free(&image);
        return 2;
    }
    // image_print("read image", image);


    struct image new_image;


    apply_filter(&new_image, image, type);


    enum write_status write_status = write_image(&new_image, res_name);
    if (write_status != WRITE_OK) {
        printf("Write error: %s\n", write_errors[write_status]);
        image_free(&image);
        image_free(&new_image);
        return 3;
    }
    // image_print("write image", new_image);

    image_free(&image);
    image_free(&new_image);
}

enum FILE_TRANSFORM_TYPE {
    ROTATE_FILE = -1,
    DEFAULT,
    SEPIA_FILE
};

//type source res angle / type source res
int main(int argc, char **argv) {
    (void) argc;
    (void) argv; // supress 'unused parameters' warning

    if (argc == 1) {
        printf("Invalid count of args. Please follow template\n"
               "\t-1 *source* *res* *angle*\n"
               "\t *filter_type* *source* *res*");
    }

    enum FILE_TRANSFORM_TYPE transform_type = atoi(argv[1]);
    if (transform_type == ROTATE_FILE) {
        if (argc != 5) {
            printf("Rotation: Invalid count of args: expected 4, inserted: %d\n", argc);
            return 1;
        }

        return rotate_file(argv[2], argv[3], argv[4]);
    } else {
        if (argc != 4) {
            printf("Filter: Invalid count of args: expected 4, inserted: %d\n", argc);
            return 1;
        }

        return filter_file(argv[2], argv[3], argv[1]);

    }

}
