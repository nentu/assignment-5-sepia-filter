#include "../include/struct/image.h"
#include "../include/struct/transform.h"
#include <stdbool.h>
#include "utils.h"

#define TRANSFORM_RULE(name, x_rule, y_rule, isRotated) \
  static size_t name##_get_x(struct image const new, size_t x, size_t y){ \
    return (x_rule) + 0*(new.width+x+y);   \
  } \
  static size_t name##_get_y(struct image const new, size_t x, size_t y){  \
    return (y_rule) + 0*(new.width+x+y); \
  }                                          \
  const struct transform_rule name##_trans_rule = {                        \
           name##_get_x,                      \
           name##_get_y,                      \
           isRotated                          \
  };

#define FILTER_RULE(name, r_rule, g_rule, b_rule) \
    uint8_t name##_r (struct pixel const source_pixel){ \
        return r_rule; \
    } \
    uint8_t name##_g (struct pixel const source_pixel){ \
        return g_rule; \
    } \
    uint8_t name##_b (struct pixel const source_pixel){ \
        return b_rule; \
    } \
    const struct filter_rule name##_filter_rule = { \
            name##_r, \
            name##_g, \
            name##_b \
    };


TRANSFORM_RULE(default, x, y, false)

TRANSFORM_RULE(right_90, (new.width - 1) - y, x, true)

TRANSFORM_RULE(left_90, y, (new.height - 1) - x, true)

TRANSFORM_RULE(turn_180, (new.width - 1) - x, (new.height - 1) - y, false)

TRANSFORM_RULE(transponce_side_diagonal, y, x, true)

TRANSFORM_RULE(x_reflect, (new.width - 1) - x, y, false)

struct transform_rule transform_list[] = {
        default_trans_rule,
        right_90_trans_rule,
        left_90_trans_rule,
        turn_180_trans_rule,
        transponce_side_diagonal_trans_rule,
        x_reflect_trans_rule
};


FILTER_RULE(default,
            source_pixel.r,
            source_pixel.g,
            source_pixel.b)

uint8_t lin_filter(double a, double b, double c, struct pixel const scr) {
    uint64_t res = a * scr.r + b * scr.g + c * scr.b;
    return min(res, 255);
}

FILTER_RULE(sepia,
            lin_filter(0.393, 0.769, 0.189, source_pixel),
            lin_filter(0.349, 0.686, 0.168, source_pixel),
            lin_filter(0.272, 0.534, 0.131, source_pixel)
)

struct filter_rule filter_list[] = {
        default_filter_rule,
        sepia_filter_rule
};

struct transform_rule get_transform_rule(enum transform_type type) {
    return transform_list[type];
}

struct filter_rule get_filter_rule(enum filter_type type) {
    return filter_list[type];
}
