#ifndef TRANSFORMSTRUCTURE_H
#define TRANSFORMSTRUCTURE_H

#include "struct/image.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

typedef size_t (*coord_transform) (struct image const new_image, size_t source_x, size_t source_y);

typedef uint8_t (*pixel_filter) (struct pixel const source_pixel);
// typedef size_t (coord_transform)(struct image const new, size_t source_x, size_t source_y);

struct transform_rule{
  coord_transform get_x;
  coord_transform get_y;
  bool is_rotated;
};

struct filter_rule{
    pixel_filter r_filer;
    pixel_filter g_filer;
    pixel_filter b_filer;
};

enum transform_type{
    DEFAULT_T = 0,
    RIGHT_90,
    TURN_180,
    LEFT_90,
    TRANSPONCE_SIDE_DIAGONAL,
    X_REFLECT
};

enum filter_type{
    DEFAULT_F = 0,
    SEPIA
};

#endif
