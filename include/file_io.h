#include "io_result_enums.h"
#include "struct/image.h"

enum read_status read_image(struct image* image, const char* file_name);

enum write_status write_image(const struct image* image, const char* file_name);
