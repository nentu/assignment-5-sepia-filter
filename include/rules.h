#include "struct/transform.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>


struct transform_rule get_transform_rule(enum transform_type type);
struct filter_rule get_filter_rule(enum filter_type type);
