 #ifndef IOENUMS_H
 #define IOENUMS_H

enum read_status  {
  READ_OK = 0,
  READ_INVALID_PATH,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
  /* коды других ошибок  */
  };

enum  write_status  {
  WRITE_OK = 0,
  WRITE_INVALID_PATH,
  WRITE_ERROR
  /* коды других ошибок  */
};

#endif
